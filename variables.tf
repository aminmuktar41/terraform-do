variable "do_token" {
  description = "DigitalOcean API token"
  type        = string
}

variable "region" {
  description = "Degitalocean region to deploy resources"
  type = string
  default = "nyc3"
}

variable "droplet_image" {
  description = "Droplet image to use"
  type = string
  default = "ubuntu-20-04-x64"
}

variable "droplet_size" {
  description = "Droplet size to use"
  type = string
  default = "s-2vcpu-2gb"
}

variable "manager_count" {
  description = "Number of manager nodes"
  type = number
  default = 1
}

variable "worker_count" {
  description = "Number of worker nodes"
  type = number
  default = 2
}
