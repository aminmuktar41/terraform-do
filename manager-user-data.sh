#!/bin/bash
apt-get update
apt-get install -y docker.io
docker swarm init --advertise-addr $(hostname -I | awk '{print $1}')
docker network create -d overlay monitor-net

# Write files
cat <<EOF > /root/prometheus.yml
${prometheus_file}
EOF

cat <<EOF > /root/alert.rules.yml
${alert_rules_file}
EOF

cat <<EOF > /root/docker-compose.yml
${docker_compose_file}
EOF

docker stack deploy -c /root/docker-compose.yml monitor