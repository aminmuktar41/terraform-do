output "manager_ips" {
  description = "Ip addresses of the manager nodes"
  value = digitalocean_droplet.manager.*.ipv4_address
}

output "worker_ips" {
  description = "IP addresses of the worker nodes"
  value = digitalocean_droplet.worker.*.ipv4_address
}


