terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.5.0"
    }
  }
}
provider "digitalocean" {
  token = var.do_token
}
# resource "digitalocean_ssh_key" "amin_ssh_key" {
#   name = "amin-ssh-key"
#   public_key = file("id_rsa.pub")
# }
data "digitalocean_ssh_key" "amin_ssh_key" {
  name = "Amin"  # Name of your existing SSH key in DigitalOcean
}

resource "digitalocean_droplet" "manager" {
  count = var.manager_count
  image = var.droplet_image
  name  = "manager-${count.index + 1}"
  region = var.region
  size = var.droplet_size
  user_data = templatefile("manager-user-data.sh", {
    docker_compose_file = file("docker-compose.yaml"),
    prometheus_file = file("prometheus.yml"),
    alert_rules_file = file("alert.rules.yml")
  })
  ssh_keys = [
    data.digitalocean_ssh_key.amin_ssh_key.fingerprint
  ]
  tags = ["docker", "manager"]
}

resource "digitalocean_droplet" "worker" {
  count = var.worker_count
  image = var.droplet_image
  name = "worker-${count.index +1}"
  region = var.region
  size = var.droplet_size
  user_data = templatefile("worker-user-data.sh", {})
  ssh_keys = [
    data.digitalocean_ssh_key.amin_ssh_key.fingerprint
  ]
  tags = ["docker", "worker"]
}
